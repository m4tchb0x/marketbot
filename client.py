#! python
import sys
import getopt
import random
import requests
import pickle
import traceback
import urllib
from bs4 import BeautifulSoup
import re
import copy
import queue
import threading
import time
import json
import zmq
import zlib

port = "55563"
context = zmq.Context()
socket = context.socket(zmq.PAIR)
socket.connect("tcp://108.162.145.185:%s" % port)

debug = False

threads = 2
maxPrice = 5

searchQueue = queue.Queue(1000)
soloQueue = queue.Queue(10)

itemCheck = {}
userAgents = []
proxies = []


class ThreadSearch(threading.Thread):
	
	def __init__(self, queue):
		threading.Thread.__init__(self)
		self.queue = queue

	def run(self):
		global maxPrice, proxies, userAgents, buyQueue, cookiesReq, itemCheck
		while True:

			reqInfo = self.queue.get()

			try:
				proxy = {
					"https": reqInfo['ip'],
					"http": reqInfo['ip']
				}

				headers = {
					'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
					'Accept-Encoding': 'gzip, deflate',
					'Accept-Language': 'en-US,en;q=0.8',
					'Cache-Control': 'max-age=0',
					'Connection':'keep-alive',
					'Host': 'steamcommunity.com',
					'Referer':'http://steamcommunity.com/market/',
					'User-Agent': random.choice(userAgents)
				}

				r = requests.get(reqInfo['url'], proxies=proxy, headers=headers, timeout=(0.2,0.5))
				
				if '"success":true' in r.text:
					jsonObj = r.json()
					#htmlObj = str(jsonObj['results_html'].encode('utf8', 'replace')).replace('\\"','"').replace('\\r','').replace('\\n','').replace('\\t','').replace('\/','/')
					soup = BeautifulSoup(jsonObj['results_html'])
					#print(jsonObj['results_html'])
					items = soup.find_all('div', class_='market_listing_row')
					output = '{!s:5s} {!s:65s} {!s:8s}'.format(reqInfo['nonProxy'],proxy,r.elapsed.total_seconds()) + '\n'
					listings = jsonObj['listinginfo']
					#print(reqInfo['nonProxy'] , proxy , r.elapsed)
					for item in items:
						itemName = item.find('a', class_='market_listing_item_name_link')['href']
						listingId = item['id'].replace('listing_sell_new_','')
						if 'http://steamcommunity.com/market/listings/730/' in itemName:
							itemName = urllib.parse.unquote(str(itemName.replace('http://steamcommunity.com/market/listings/730/',''))).replace('★','').replace(u"\u2122", '').replace(u"\u9f8d", '').replace(u"\u738b",'').strip()
							itemName = str(itemName)
							priceRe = re.findall("\d+.\d+", item.find('span', class_='market_listing_price_with_fee').get_text())
							if len(priceRe) > 0:
								itemPrice = float(priceRe[0])
							else:
								itemPrice = -1
							output += '{:<60} {:.2f} '.format(itemName,itemPrice)
							if itemPrice > 0 and itemName in itemCheck:
								itemToCheck = itemCheck[itemName]
								itemMinPrice = float(itemToCheck['minPrice'])
								output += ' b@ {:.2f}\n'.format(itemMinPrice)
								if itemPrice <= itemMinPrice:
									listingPrice = int(listings[listing]['converted_price_per_unit'])
									listingFee = int(listings[listing]['converted_fee_per_unit'])
									listingTotal = listingPrice + listingFee
								
									listingInfo= {}
									listingInfo['listing'] = listingId
									listingInfo['listingPrice'] = listingPrice
									listingInfo['listingFee'] = listingFee
									listingInfo['listingTotal'] = listingTotal
									listingInfo['ref'] = 'http://steamcommunity.com/market/'
									listingInfo['itemName'] = itemName
									send_zipped_pickle(socket, listingInfo)
							else:
								output += '\n'
					output += '------------------------------------------------------------------------------\r\n'
					print(output)
			except Exception as e:
				if 'Max retries exceeded' in str(e):
					proxies[reqInfo['index']]['cooldownTill'] = time.time() + 900
					print('Max retries exceeded on #', reqInfo['index'], reqInfo['ip'], ', cooldown till', proxies[reqInfo['index']]['cooldownTill'])
				else:
					print(e)
				# 	print(traceback.print_exc(file=sys.stdout))
				#print("Unexpected error:", sys.exc_info()[0], reqInfo['ip'])
				#proxies.append(reqInfo)



			if reqInfo['nonProxy'] == True:
				time.sleep(0.1)
			# signals to queue job is done
			self.queue.task_done()

def checkMarket():
	#make search threads
	for i in range(0,threads):
		t = ThreadSearch(searchQueue)
		t.daemon = True
		t.start()

	#Non Proxy Thread
	t = ThreadSearch(soloQueue)
	t.daemon = True
	t.start()

	#populate queue with data 
	while True:
		#ip = random.choice(proxies)
		for index, proxy in enumerate(proxies):
			if hasattr(proxy, 'cooldownTill'):
				if proxy['cooldownTill'] > time.time():
					print('Skipping: ', proxy)
					continue
				else:
					delattr(proxy, 'cooldownTill')
			request = {}
			request['index'] = index
			request['ip'] = proxy['ip']
			request['nonProxy'] = False

			soloRequest = {}
			soloRequest['index'] = index
			soloRequest['ip'] = {}
			soloRequest['nonProxy'] = True
			
			if debug:
				request['url'] = 'http://steamcommunity.com/market/search/render/?start=0&count=1&q=&category_730_ItemSet%5B%5D=any&category_730_TournamentTeam%5B%5D=any&category_730_Weapon%5B%5D=any&category_730_Rarity%5B%5D=tag_Rarity_Common_Weapon&appid=730#p1_price_asc'
			else:
				request['url'] = 'http://steamcommunity.com/market/recent?country=CA&language=english&currency=20'
				# request['url'] = 'http://steamcommunity.com/market/search/render/?start=0&count=1&q=&category_730_ItemSet%5B0%5D=any&category_730_TournamentTeam%5B0%5D=any&category_730_Weapon%5B0%5D=any&category_730_Rarity%5B0%5D=tag_Rarity_Legendary_Weapon&category_730_Rarity%5B1%5D=tag_Rarity_Ancient_Weapon&appid=730&sort_column=price&sort_dir=asc&appid=730&currency=5&language=english'
			
			soloRequest['url'] = request['url']
			# searchQueue.put(request)
			if not(soloQueue.full()):
				soloQueue.put(soloRequest)

	#wait on the queue until everything has been processed     
	searchQueue.join()
	soloQueue.join()

def send_zipped_pickle(socket, obj, flags=0, protocol=-1):
	"""pickle an object, and zip the pickle before sending it"""
	p = pickle.dumps(obj, protocol)
	z = zlib.compress(p)
	return socket.send(z, flags=flags)

def recv_zipped_pickle(socket, flags=0, protocol=-1):
	"""inverse of send_zipped_pickle"""
	z = socket.recv(flags)
	p = zlib.decompress(z)
	return pickle.loads(p)

def usage():
	print('No Help Yet')

def main():
	global proxies, debug, threads, userAgents, itemCheck

	try:
		opts, args = getopt.getopt(sys.argv[1:], "hd", ["help", "debug"])
	except getopt.GetoptError:
		usage()
		sys.exit(2)

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit()
		elif opt == '-d':
			debug = True
			threads = 1
		elif opt in ("-l", "--login"):
			login()

	#take 100 random userAgents from file
	with open('useragents.txt', "r") as f:
		userAgentText = f.read()
		userAgents = userAgentText.split('\n')
		random.shuffle(userAgents)
		userAgents = userAgents[:100]

	itemCheck = pickle.load(open("itemsDict.dat", "rb"))
	print('Items: ', len(itemCheck))

	f = open("proxies.dat", "rb")
	ipports = pickle.load(f)
	for ipport in ipports:
		proxies.append({'ip' : ipport })
	random.shuffle(proxies)
	print('Proxy Count: ', len(ipports), '\n')

	checkMarket()
	

if __name__ =='__main__':main()