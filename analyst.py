#! python
import sys
import getopt
import random
import configparser
import requests
import pickle
import traceback
from bs4 import BeautifulSoup
import re
import copy
import queue
import threading
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

config = configparser.ConfigParser()
config.read("config.ini")

dcap = dict(DesiredCapabilities.PHANTOMJS)
dcap["phantomjs.page.settings.userAgent"] = (
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36"
)

debug = False
threads = 20

itemsSkipped = []
itemsFound = []
itemsDict = {}

searchQueue = queue.Queue(20000)

userAgents = []
proxies = []
proxyIndex = 0

cookies = []
cookiesReq = {}

itemsTotal = 0
itemsScanned = 0

class ThreadSearch(threading.Thread):
	
	def __init__(self, queue):
		threading.Thread.__init__(self)
		self.queue = queue

	def run(self):
		global proxies, proxyIndex, userAgents, searchQueue, cookiesReq, itemsScanned, itemsTotal, itemsDict, itemsSkipped
		while True:
			item = self.queue.get()
			#print('starting', item['url'] )
			try:
				proxy = {
					"https": proxies[proxyIndex]['ip'],
					"http": proxies[proxyIndex]['ip']
				}

				headers = {
					'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
					'Accept-Encoding': 'gzip, deflate',
					'Accept-Language': 'en-US,en;q=0.8',
					'Cache-Control': 'max-age=0',
					'Connection':'keep-alive',
					'Host': 'csgo.steamanalyst.com',
					'Referer': 'http://csgo.steamanalyst.com/list.php',
					'User-Agent': random.choice(userAgents)
				}

				url = 'http://csgo.steamanalyst.com' + str(item['url'])
				# print(url)
				# print(proxy)
				# print(headers)
				r = requests.get(url , proxies=proxy, headers=headers, timeout=(4,10))

				html = r.text
				# with open("test.html", "w",encoding='utf-8') as myfile:
				# 	myfile.write(html)
				itemsScanned += 1
				
				inspectIds = re.findall(r'(?<=csgo_econ_action_preview%20M)(.*)(?=">Inspect</a>)',html)
				if len(inspectIds) > 0:
					steamId = inspectIds[0].replace('D', 'A').split('A')[1]
					# print(steamId)
					itemsDict[steamId] = item
					# if itemsScanned % 100 == 0:
					# 	print('Scanned:' , itemsScanned, 'out of', itemsTotal)
				else:
					if item not in itemsSkipped:
						itemsSkipped.append(item)
					print('Skipping:' , item['url'])
				
			except Exception as e:
				if proxyIndex == len(proxies) - 1:
					proxyIndex = 0
				else:
					proxyIndex += 1
				searchQueue.put(item)
				print(sys.exc_info()[0])
				# print(traceback.print_exc(file=sys.stdout))
				#sys.exit(1)
				#print(traceback.print_exc(file=sys.stdout))
				#print("Unexpected error:", sys.exc_info()[0], reqInfo['ip'])
				#proxies.append(reqInfo)

			# signals to queue job is done
			self.queue.task_done()

def getListAndCookies():
	print('Getting Listing and Cookies... ')
	driver = webdriver.PhantomJS(executable_path= config['Config']['phantomjs'] , desired_capabilities=dcap) # or add to your PATH
	driver.set_window_size(1024, 768) # optional
	driver.get('http://csgo.steamanalyst.com/list.php')
	driver.find_element_by_xpath("//div[@id='currency']").click()
	time.sleep(3)

	driver.find_element_by_xpath("//div[@id='currency']//li[contains(text(),'CAD')]").click()
	time.sleep(3)

	driver.find_element_by_xpath("//select[@name='list_length']//option[contains(text(),'All')]").click()
	time.sleep(10)
	for cookie in driver.get_cookies():
		cookiesReq[cookie['name']] = cookie['value']

	json = driver.execute_script("return $('#list').dataTable().fnGetData();")
	for i in json:
		#'Knife', '/id/2162/Karambit-Slaughter-Field-Tested', 'Karambit', 'Slaughter', 'Field-Tested', 'ft', '526.96', '494.29</a>', '26.12', '11', 12, '<span class="stable">-1.67</span>', '42.72279'
		item = {}
		item['url'] = i[1]
		item['type'] = i[0]
		item['name'] = i[2].replace("\u2122", '').replace("\u9f8d", '').replace("\u738b",'')
		item['skin'] = i[3]
		item['condition'] = i[4]
		if 'N/A' != i[6]:
			item['price'] = float(i[6])
		else:
			item['price'] = float(i[7].replace('</a>',''))
		if 'N/A' != i[7]:
			item['avgPrice'] = float(i[7].replace('</a>',''))
		if 'N/A' != i[9]:
			item['unitsSold'] = int(i[9])
		if 'N/A' != i[10]:
			item['unitsListed'] = int(i[10])
		if 'N/A' != i[11]:
			trend = i[11].replace('<span class="','').replace('</span>','').split('">')
			item['trend'] = trend[0]
			item['trendPercent'] = float(trend[1].replace(',',''))
		if '' != i[12]:
			item['volatility'] = float(i[12])
		if item['type'] not in ['Pass', 'Gift' , 'Music Kit', 'Container', 'Key' , 'Sticker']:
			itemsFound.append(item)
	#driver.save_screenshot('analyze.png') # save a screenshot to disk
	print('Done! Found', len(itemsFound), 'items.')

def usage():
	print('No Help Yet')

def main():
	global cookies, proxies, cookiesReq, threads, config, userAgents, itemsFound, itemsDict, itemsScanned, itemsTotal, itemsDict, itemsSkipped
	
	#take 100 random userAgents from file
	with open('useragents.txt', "r") as f:
		userAgentText = f.read()
		userAgents = userAgentText.split('\n')
		random.shuffle(userAgents)
		userAgents = userAgents[:100]

	#get all proxies
	f = open("proxies.dat", "rb")
	ipports = pickle.load(f)
	for ipport in ipports:
		proxies.append({'ip' : ipport })
	random.shuffle(proxies)
	print('Proxy Count: ', len(ipports), '\n')

	getListAndCookies()

	# print('Starting to get Steam Market ID for each item...')
	# #make search threads
	# for i in range(0,threads):
	# 	t = ThreadSearch(searchQueue)
	# 	t.daemon = True
	# 	t.start()

	# #populate queue with data 
	# for item in itemsFound:
	# 	searchQueue.put(item)
	# #wait on the queue until everything has been processed     
	# searchQueue.join()

	#populate queue with data
	with open("itemPrices.txt", "w",encoding='utf-8') as myfile:
		for item in itemsFound:
			itemId = item['name']
			if item['skin'] != 'Vanilla':
				itemId += ' | ' + item['skin'] + ' (' + item['condition'] + ')'
			minPrice = min(item['price'], item['avgPrice']) * (0.3 * min(item['price'], item['avgPrice'], 30) / 30)
			item['minPrice'] = minPrice
			itemsDict[itemId] = item
			myfile.write('{:<80} {:3.2f} {:3.2f} {:3.2f}'.format(itemId,item['price'],item['avgPrice'],minPrice) + '\n')

	#print('items in itemsFound:', len(itemsFound))
	print('items in itemsDict:', len(itemsDict))
	# print('items in itemsSkipped:', len(itemsSkipped))

	# with open("itemskip.json", "w",encoding='utf-8') as myfile:
	# 	myfile.write(str(itemsSkipped))
	with open("itemDict.json", "w",encoding='utf-8') as myfile:
		json.dump(itemsDict, myfile)
	with open('itemsDict.dat', "wb") as myfile:
		pickle.dump(itemsDict, myfile)

	

	# checkMarket()
	

if __name__ =='__main__':main()