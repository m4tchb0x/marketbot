#! python
import sys
import getopt
import random
import configparser
import requests
import pickle
import traceback
from bs4 import BeautifulSoup
import re
import copy
import queue
import threading
import time
import json
import zmq
import zlib

port = "55563"
context = zmq.Context()
socket = context.socket(zmq.PAIR)
socket.bind("tcp://*:%s" % port)

debug = False
buyThreads = 1

buyQueue = queue.Queue(10)
buyIds = []

cookies = []
cookiesReq = {}

class ThreadBuy(threading.Thread):
	
	def __init__(self, queue):
		threading.Thread.__init__(self)
		self.queue = queue

	def run(self): #buy(self, listing, listingPrice, listingFee, listingTotal, ref, itemName):
		global cookiesReq, debug, config, buyIds
		while True:

			reqInfo = self.queue.get()

			listing = reqInfo['listing']
			listingPrice = reqInfo['listingPrice']
			listingFee = reqInfo['listingFee']
			listingTotal = reqInfo['listingTotal']
			ref = reqInfo['ref']
			itemName = reqInfo['itemName']

			if listing not in buyIds:
				try:
					#Ready Up the request
					payload = 'sessionid='+cookiesReq['sessionid']+'&currency=20&subtotal='+str(listingPrice)+'&fee='+str(listingFee)+'&total='+str(listingTotal)+'&quantity=1'
					headers = {
						'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
						'Accept-Language': 'en-US,en;q=0.5',
						'Accept-Encoding': 'gzip, deflate',
						'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36',
						'Referer': ref,
						'Origin': 'http://steamcommunity.com',
						'Pragma': 'no-cache',
						'Cache-Control': 'no-cache',
						'Host': 'steamcommunity.com'
					}
					url = 'https://steamcommunity.com/market/buylisting/' + listing

					#Send Request
					postReq = requests.post(url, payload, cookies=cookiesReq, headers=headers)

					#Print request info and save the log to files
					print('Headers: ', postReq.headers )
					print()
					print('Headers: ', postReq.text )
					print()
					print('RequestHeaders: ', postReq.request.headers )
					print()
					print('Code: ', postReq.status_code )
					with open(config['Config']['dropbox'] + "bought.txt", "a") as myfile:
						myfile.write('ATTEMPTING {:<60} {:.2f}'.format(listing,listingTotal/100))
						myfile.write('\nSending: ' + url + '\n' + payload + '\n' + str(cookiesReq))
						#pickle.dump(cookiesReq, myfile)
						myfile.write('\nHeaders: ' + str(postReq.headers))
						myfile.write('\nText: ' + postReq.text)
						myfile.write('\nRequestHeaders: ' + str(postReq.request.headers))
						myfile.write('\nCode: ' + str(postReq.status_code)  + '\n----------------------------------------------------------------\n')
					with open(config['Config']['dropbox'] + "found.txt", "a") as myfile:
						myfile.write('{:<60} {:.2f}'.format(itemName,listingTotal/100) + '\n')
					
					buyIds.append(listing)
					#Exit after first buy if its debug
					if debug:
						sys.exit(0)

				except Exception as e:
					with open(config['Config']['dropbox'] + "buy-error.txt", "a") as myfile:
						myfile.write('{:<60} {:.2f}\n'.format(itemName,listingTotal/100) + str(e) + '\n')
					print(e)

			self.queue.task_done()

def send_zipped_pickle(socket, obj, flags=0, protocol=-1):
	"""pickle an object, and zip the pickle before sending it"""
	p = pickle.dumps(obj, protocol)
	z = zlib.compress(p)
	return socket.send(z, flags=flags)

def recv_zipped_pickle(socket, flags=0, protocol=-1):
	"""inverse of send_zipped_pickle"""
	z = socket.recv(flags)
	p = zlib.decompress(z)
	return pickle.loads(p)

def usage():
	print('No Help Yet')

def main():
	global cookies, cookiesReq, debug, config, userAgents, socket

	try:
		opts, args = getopt.getopt(sys.argv[1:], "hld", ["help", "login", "debug"])
	except getopt.GetoptError:
		usage()
		sys.exit(2)

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit()
		elif opt == '-d':
			debug = True
		elif opt in ("-l", "--login"):
			login()

	#take 100 random userAgents from file
	with open('useragents.txt', "r") as f:
		userAgentText = f.read()
		userAgents = userAgentText.split('\n')
		random.shuffle(userAgents)
		userAgents = userAgents[:100]

	cookies = pickle.load(open("cookies.pkl", "rb"))
	for cookie in cookies:
		cookiesReq[cookie['name']] = cookie['value']
	print('Cookies: ', cookiesReq, '\n')

	#make buy threads
	for i in range(0,buyThreads):
		t = ThreadBuy(buyQueue)
		t.daemon = True
		t.start()

	while True:
		buyInfo = recv_zipped_pickle(socket)
		print(buyInfo)
		buyQueue.put(buyInfo)
		time.sleep(0.01)
	

if __name__ =='__main__':main()