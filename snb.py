#! python
import sys
import getopt
import random
import configparser
import requests
import pickle
import traceback
import urllib
from bs4 import BeautifulSoup
import re
import copy
import queue
import threading
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

config = configparser.ConfigParser()
config.read("config.ini")

dcap = dict(DesiredCapabilities.PHANTOMJS)
dcap["phantomjs.page.settings.userAgent"] = (
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36"
)

debug = False
threads = 1
buyThreads = 1
maxPrice = 5.0

searchQueue = queue.Queue(100)
buyQueue = queue.Queue(10)
soloQueue = queue.Queue(10)
buyIds = []

itemCheck = {}

userAgents = []
proxies = []
cookies = []
cookiesReq = {}

class ThreadBuy(threading.Thread):
	
	def __init__(self, queue):
		threading.Thread.__init__(self)
		self.queue = queue

	def run(self): #buy(self, listing, listingPrice, listingFee, listingTotal, ref, itemName):
		global cookiesReq, debug, config, buyIds
		while True:

			reqInfo = self.queue.get()

			listing = reqInfo['listing']
			listingPrice = reqInfo['listingPrice']
			listingFee = reqInfo['listingFee']
			listingTotal = reqInfo['listingTotal']
			ref = reqInfo['ref']
			itemName = reqInfo['itemName']

			if listing not in buyIds:
				try:
					#payload2 = {'sessionid': cookiesReq['sessionid'] ,'currency': 20, 'subtotal': listingPrice, 'fee': listingFee, 'total': listingTotal, 'quantity': 1 }

					#Ready Up the request
					payload = 'sessionid='+cookiesReq['sessionid']+'&currency=20&subtotal='+str(listingPrice)+'&fee='+str(listingFee)+'&total='+str(listingTotal)+'&quantity=1'
					headers = {
						'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
						'Accept-Language': 'en-US,en;q=0.5',
						'Accept-Encoding': 'gzip, deflate',
						'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36',
						'Referer': ref,
						'Origin': 'http://steamcommunity.com',
						'Pragma': 'no-cache',
						'Cache-Control': 'no-cache',
						'Host': 'steamcommunity.com'
					}
					url = 'https://steamcommunity.com/market/buylisting/' + listing

					#Send Request
					postReq = requests.post(url, payload, cookies=cookiesReq, headers=headers)

					#Print request info and save the log to files
					print('Headers: ', postReq.headers )
					print()
					print('Headers: ', postReq.text )
					print()
					print('RequestHeaders: ', postReq.request.headers )
					print()
					print('Code: ', postReq.status_code )
					with open(config['Config']['dropbox'] + "bought.txt", "a") as myfile:
						myfile.write('ATTEMPTING {:<60} {:.2f}'.format(listing,listingTotal/100))
						myfile.write('\nSending: ' + url + '\n' + payload + '\n' + str(cookiesReq))
						#pickle.dump(cookiesReq, myfile)
						myfile.write('\nHeaders: ' + str(postReq.headers))
						myfile.write('\nText: ' + postReq.text)
						myfile.write('\nRequestHeaders: ' + str(postReq.request.headers))
						myfile.write('\nCode: ' + str(postReq.status_code)  + '\n----------------------------------------------------------------\n')
					with open(config['Config']['dropbox'] + "found.txt", "a") as myfile:
						myfile.write('{:<60} {:.2f}'.format(itemName,listingTotal/100) + '\n')
					
					buyIds.append(listing)
					#Exit after first buy if its debug
					if debug:
						sys.exit(0)

				except Exception as e:
					with open(config['Config']['dropbox'] + "buy-error.txt", "a") as myfile:
						myfile.write('{:<60} {:.2f}\n'.format(itemName,listingTotal/100) + str(e) + '\n')
					print(e)

			self.queue.task_done()


class ThreadSearch(threading.Thread):
	
	def __init__(self, queue):
		threading.Thread.__init__(self)
		self.queue = queue

	def run(self):
		global maxPrice, proxies, userAgents, buyQueue, cookiesReq, itemCheck
		while True:

			reqInfo = self.queue.get()

			try:
				proxy = {
					"https": reqInfo['ip'],
					"http": reqInfo['ip']
				}

				headers = {
					'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
					'Accept-Encoding': 'gzip, deflate',
					'Accept-Language': 'en-US,en;q=0.8',
					'Cache-Control': 'max-age=0',
					'Connection':'keep-alive',
					'Host': 'steamcommunity.com',
					'Referer':'http://steamcommunity.com/market/',
					'User-Agent': random.choice(userAgents)
				}

				r = requests.get(reqInfo['url'], proxies=proxy, headers=headers, timeout=(0.2,0.5))
				
				if '"success":true' in r.text:
					jsonObj = r.json()
					#htmlObj = str(jsonObj['results_html'].encode('utf8', 'replace')).replace('\\"','"').replace('\\r','').replace('\\n','').replace('\\t','').replace('\/','/')
					soup = BeautifulSoup(jsonObj['results_html'])
					#print(jsonObj['results_html'])
					items = soup.find_all('div', class_='market_listing_row')
					output = '{!s:5s} {!s:65s} {!s:8s}'.format(reqInfo['nonProxy'],proxy,r.elapsed.total_seconds()) + '\n'
					listings = jsonObj['listinginfo']
					#print(reqInfo['nonProxy'] , proxy , r.elapsed)
					for item in items:
						itemName = item.find('a', class_='market_listing_item_name_link')['href']
						listingId = item['id'].replace('listing_sell_new_','')
						if 'http://steamcommunity.com/market/listings/730/' in itemName:
							itemName = urllib.parse.unquote(str(itemName.replace('http://steamcommunity.com/market/listings/730/',''))).replace('★','').replace(u"\u2122", '').replace(u"\u9f8d", '').replace(u"\u738b",'').strip()
							itemName = str(itemName)
							priceRe = re.findall("\d+.\d+", item.find('span', class_='market_listing_price_with_fee').get_text())
							if len(priceRe) > 0:
								itemPrice = float(priceRe[0])
							else:
								itemPrice = -1
							output += '{:<60} {:.2f} '.format(itemName,itemPrice)
							if itemPrice > 0 and itemName in itemCheck:
								itemToCheck = itemCheck[itemName]
								itemMinPrice = float(itemToCheck['minPrice'])
								output += ' b@ {:.2f}\n'.format(itemMinPrice)
								if itemPrice <= itemMinPrice:
									listingPrice = int(listings[listing]['converted_price_per_unit'])
									listingFee = int(listings[listing]['converted_fee_per_unit'])
									listingTotal = listingPrice + listingFee
								
									listingInfo= {}
									listingInfo['listing'] = listingId
									listingInfo['listingPrice'] = listingPrice
									listingInfo['listingFee'] = listingFee
									listingInfo['listingTotal'] = listingTotal
									listingInfo['ref'] = 'http://steamcommunity.com/market/'
									listingInfo['itemName'] = itemName
									buyQueue.put(listingInfo)
							else:
								output += '\n'
					output += '------------------------------------------------------------------------------\r\n'
					print(output)
			except Exception as e:
				if 'Max retries exceeded' in str(e):
					proxies[reqInfo['index']]['cooldownTill'] = time.time() + 900
					print('Max retries exceeded on #', reqInfo['index'], reqInfo['ip'], ', cooldown till', proxies[reqInfo['index']]['cooldownTill'])
				else:
					print(e)
				# 	print(traceback.print_exc(file=sys.stdout))
				#print("Unexpected error:", sys.exc_info()[0], reqInfo['ip'])
				#proxies.append(reqInfo)



			if reqInfo['nonProxy'] == True:
				time.sleep(0.1)
			# signals to queue job is done
			self.queue.task_done()

def checkMarket():
	#make search threads
	for i in range(0,threads):
		t = ThreadSearch(searchQueue)
		t.daemon = True
		t.start()
	#make buy threads
	for i in range(0,buyThreads):
		t = ThreadBuy(buyQueue)
		t.daemon = True
		t.start()

	#Non Proxy Thread
	t = ThreadSearch(soloQueue)
	t.daemon = True
	t.start()

	#populate queue with data 
	while True:
		#ip = random.choice(proxies)
		for index, proxy in enumerate(proxies):
			if hasattr(proxy, 'cooldownTill'):
				if proxy['cooldownTill'] > time.time():
					print('Skipping: ', proxy)
					continue
				else:
					delattr(proxy, 'cooldownTill')
			request = {}
			request['index'] = index
			request['ip'] = proxy['ip']
			request['nonProxy'] = False

			soloRequest = {}
			soloRequest['index'] = index
			soloRequest['ip'] = {}
			soloRequest['nonProxy'] = True
			
			if debug:
				request['url'] = 'http://steamcommunity.com/market/search/render/?start=0&count=1&q=&category_730_ItemSet%5B%5D=any&category_730_TournamentTeam%5B%5D=any&category_730_Weapon%5B%5D=any&category_730_Rarity%5B%5D=tag_Rarity_Common_Weapon&appid=730#p1_price_asc'
			else:
				request['url'] = 'http://steamcommunity.com/market/recent?country=CA&language=english&currency=20'
				# request['url'] = 'http://steamcommunity.com/market/search/render/?start=0&count=1&q=&category_730_ItemSet%5B0%5D=any&category_730_TournamentTeam%5B0%5D=any&category_730_Weapon%5B0%5D=any&category_730_Rarity%5B0%5D=tag_Rarity_Legendary_Weapon&category_730_Rarity%5B1%5D=tag_Rarity_Ancient_Weapon&appid=730&sort_column=price&sort_dir=asc&appid=730&currency=5&language=english'
			
			soloRequest['url'] = request['url']
			# searchQueue.put(request)
			if not(soloQueue.full()):
				soloQueue.put(soloRequest)

	#wait on the queue until everything has been processed     
	searchQueue.join()
	soloQueue.join()

def login():

	driver = webdriver.PhantomJS(executable_path= config['Config']['phantomjs'] , desired_capabilities=dcap) # or add to your PATH
	driver.set_window_size(1024, 768) # optional
	driver.get('https://steamcommunity.com/')
	cookies = pickle.load(open("cookies.pkl", "rb"))
	for cookie in cookies:
		driver.add_cookie(cookie)
	driver.get('https://steamcommunity.com/market/')
	time.sleep(2)
	driver.save_screenshot(config['Config']['dropbox'] + 'beforeLogout.png')
	driver.execute_script("Logout()")
	time.sleep(4)
	driver.get('https://steamcommunity.com/login/home/?goto=0')
	time.sleep(4)
	usernameEle = driver.find_element_by_id('steamAccountName')
	usernameEle.send_keys(config['Config']['username'])
	passwordEle = driver.find_element_by_id('steamPassword')
	passwordEle.send_keys(config['Config']['password'])
	time.sleep(3)
	driver.save_screenshot(config['Config']['dropbox'] + 'formfilled.png')

	html = driver.execute_script("return document.getElementsByTagName('html')[0].innerHTML")

	if 'id="captcha_entry" style="display: none;"' not in html:
		time.sleep(3)
		driver.save_screenshot(config['Config']['dropbox'] + 'auth.png')
		driver.find_element_by_id('input_captcha').send_keys(input("Captcha: "))
	if 'remember_login' in html:
		remember = driver.find_element_by_id('remember_login')
		remember.click()
	#driver.save_screenshot(config['Config']['dropbox'] + 'screen2.png') # save a screenshot to disk
	driver.find_element_by_id("SteamLogin").click()
	time.sleep(20)

	try:
		authCode = driver.find_element_by_id('authcode')
		driver.save_screenshot(config['Config']['dropbox'] + 'auth.png') # save a screenshot to disk
		authCode.send_keys(input("Auth Code: "))
		friName = driver.find_element_by_id('friendlyname')
		friName.send_keys(input("Friendly Name: "))
		driver.find_element_by_class_name("auth_button").click()
		time.sleep(8)
		driver.find_element_by_id('success_continue_btn').click()
		time.sleep(10)
	except:
		pass
	
	driver.get('https://steamcommunity.com/market/')
	time.sleep(4)
	pickle.dump( driver.get_cookies() , open("cookies.pkl","wb"))
	driver.save_screenshot(config['Config']['dropbox'] + 'finished.png')
	#driver.save_screenshot(config['Config']['dropbox'] + 'screen.png') # save a screenshot to disk

def screenshotmarket():
	driver = webdriver.PhantomJS(executable_path= config['Config']['phantomjs'] , desired_capabilities=dcap) # or add to your PATH
	driver.set_window_size(1024, 768) # optional
	driver.get('https://steamcommunity.com/')
	cookies = pickle.load(open("cookies.pkl", "rb"))
	for cookie in cookies:
		driver.add_cookie(cookie)
	driver.get('https://steamcommunity.com/market/')
	time.sleep(2)
	driver.save_screenshot(config['Config']['dropbox'] + 'ssm.png')

def usage():
	print('No Help Yet')

def main():
	global cookies, proxies, cookiesReq, debug, threads, config, userAgents, itemCheck
	# screenshotmarket()
	# sys.exit()

	try:
		opts, args = getopt.getopt(sys.argv[1:], "hld", ["help", "login", "debug"])
	except getopt.GetoptError:
		usage()
		sys.exit(2)

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit()
		elif opt == '-d':
			debug = True
			threads = 1
		elif opt in ("-l", "--login"):
			login()

	#take 100 random userAgents from file
	with open('useragents.txt', "r") as f:
		userAgentText = f.read()
		userAgents = userAgentText.split('\n')
		random.shuffle(userAgents)
		userAgents = userAgents[:100]

	itemCheck = pickle.load(open("itemsDict.dat", "rb"))
	print('Items: ', len(itemCheck))
	# with open("dictCheck.txt", "w") as myfile:
	# 	for key in itemCheck.keys():
	# 		print(key)
	# 		#myfile.write(key + '\n')

	cookies = pickle.load(open("cookies.pkl", "rb"))
	for cookie in cookies:
		cookiesReq[cookie['name']] = cookie['value']
	print('Cookies: ', cookiesReq, '\n')

	f = open("proxies.dat", "rb")
	ipports = pickle.load(f)
	for ipport in ipports:
		proxies.append({'ip' : ipport })
	random.shuffle(proxies)
	print('Proxy Count: ', len(ipports), '\n')

	checkMarket()
	

if __name__ =='__main__':main()