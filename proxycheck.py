#! python
import sys
import getopt
import requests
import pickle
import traceback
from bs4 import BeautifulSoup
import re
import queue
import threading
import time
import json

threads = 50
getNewIpList = True
queue = queue.Queue(1000)

proxyList = []
#proxy = { "https": '128.199.68.82:3128', "http": '128.199.68.82:3128' } #CZ
proxy = { "https": '202.171.253.84:85', "http": '202.171.253.84:85' }

anonCheck = False
timeout = (1,2)

ipscount = 0
ipstested = 0
ipsgood = 0

ips = []
ipsWorking = []

class ThreadUrl(threading.Thread):
	def __init__(self, queue):
		threading.Thread.__init__(self)
		self.queue = queue

	def run(self):
		global ipsWorking, ipstested, ipsgood, ipscount, anonCheck, timeout
		while True:
			#grabs host from queue
			ipInfo = self.queue.get()
			try:
				proxies = {
					"https": ipInfo,
					"http": ipInfo
				}

				if anonCheck:
					r = requests.get("http://csgo.bojan.pw/headers.php", proxies=proxies, timeout=timeout)
					if '108.162.145.185' in r.text:
						print('- {:<30} {:6d} {:6d} {:6d}'.format(ipInfo, ipsgood, ipstested, ipscount))
					elif '.' in r.text:
						r = requests.get("http://steamcommunity.com/market/search/render/?query=knife&start=0&count=10&search_descriptions=0&sort_column=price&sort_dir=asc&appid=730&currency=5&language=english", proxies=proxies, timeout=timeout)
						if '"success":true' in r.text:
							print('+ {:<30} {:6d} {:6d} {:6d}'.format(ipInfo, ipsgood, ipstested, ipscount))
							ipsgood+=1
							ipsWorking.append(ipInfo)
				else:
					r = requests.get("http://steamcommunity.com/market/search/render/?query=knife&start=0&count=10&search_descriptions=0&sort_column=price&sort_dir=asc&appid=730&currency=5&language=english", proxies=proxies, timeout=timeout)
					if r.status_code == 200:
						print('+ {:<30} {:6d} {:6d} {:6d}'.format(ipInfo, ipsgood, ipstested, ipscount))
						ipsgood+=1
						ipsWorking.append(ipInfo)


			except Exception as e:
				print('E {:<30} {:6d} {:6d} {:6d}'.format(ipInfo, ipsgood, ipstested, ipscount))
				#print("Unexpected error:", sys.exc_info()[0], ipInfo['ip'])
				#print(e)
				#print(traceback.print_exc(file=sys.stdout))
				#ipsWorking.append(ipInfo)

			# signals to queue job is done
			ipstested+=1
			self.queue.task_done()


def loopHideMyAss():
	#print('Scraping: HideMyAss')
	#Get List
	#for i in range(1, 28):
	for i in range(1, 29):
		r = requests.get('http://proxylist.hidemyass.com/search-1304811/' + str(i), proxies=proxy)
		soup = BeautifulSoup(r.text.replace('div','span'))
		rows = soup.find(id="listable").find('tbody').find_all('tr')
		for row in rows:
			cols = row.find_all('td')
			
			#get styles for tr, place inline in mstyles and none in hstyles
			styles = cols[1].find('style')
			mstyles = re.findall('(?<=.)(.+)(?={display:inline})',styles.get_text())
			hstyles = re.findall('(?<=.)(.+)(?={display:none})',styles.get_text())
			#print(mstyles)
			#print(hstyles)

			#little regex magic to counter the trick they tried
			ipspan = str(cols[1].find('span'))
			ipspan = re.sub(r'(\.)(?=<s)', r'<span style="display: inline">\1</span>', ipspan)
			ipspan = re.sub(r'(\d+)(?=<s)', r'<span style="display: inline">\1</span>', ipspan)
			ipspan = re.sub(r'(\.)(?=<s)', r'<span style="display: inline">\1</span>', ipspan)
			ipspan = re.sub(r'(\d+)(?=<s)', r'<span style="display: inline">\1</span>', ipspan)
			ipspan = re.sub(r'(?<=</span>)(\d+)(?=</s)', r'<span style="display: inline">\1</span>', ipspan)
			ipspan = re.sub(r'(?<=</span>)(\d+\.\d+)(?=</s)', r'<span style="display: inline">\1</span>', ipspan)

			#ipspan = re.sub(r'(\d*\.*\d*)(?=<s)', r'<span style="display: inline">\1</span>', ipspan)
			#ipspan = re.sub(r'(?<=</span>)(\d*\.*\d*)(?=</s)', r'<span style="display: inline">\1</span>', ipspan)
			
			ipspan = BeautifulSoup(ipspan)

			ip = ''
			for span in ipspan.find('span').find_all('span'):
				#print(span)
				#go though each span and determine if its actually viewable
				if (span.get('class') is not None and (str(span.get('class')[0]) not in hstyles)) or 'display: inline' in str(span):
					ip+=span.get_text()
			#append port
			ipport = (ip + ':' + cols[2].get_text()).replace('\n', '')
			ip = ipport
			if ip not in ips:
				ips.append(ip)
			print('HideMyAss: ',ipport)

def loopCheckerProxy():
	#print('Scraping: CheckerProxy')
	r = requests.get('http://checkerproxy.net/all_proxy', proxies=proxy)
	#r = requests.get('http://proxylist.hidemyass.com/search-1291970#listable')
	soup = BeautifulSoup(r.text)
	rows = soup.find(id="result-box-table").find('tbody').find_all('tr')
	for row in rows:
		ipport = row.find('td', class_='proxy-ipport').text.strip()
		protocol = row.find('td', class_='proxy-type-1').text.strip()
		security = row.find('td', class_='proxy-type-2').text.strip()
		post = row.find('td', class_='proxy-post').text.strip()
		proxcookie = row.find('td', class_='proxy-cookie').text.strip()
		proxref = row.find('td', class_='proxy-referer').text.strip()
		if post == '+' and proxcookie == '+' and proxref == '+' and 'nonymous' in security :
			ip = ipport
			if ip not in ips:
				ips.append(ip)
			print('CheckerProxy: ',ipport)

def loopRebro():
	#print('Scraping: Rebro')
	r = requests.get('http://rebro.weebly.com/uploads/2/7/3/7/27378307/rebroproxy-all-113326062014.txt', proxies=proxy)
	ipList = r.text.split('\n')
	protocol = 'HTTPS'
	for ipport in ipList:
		ipport = ipport.strip()
		ip = ipport
		if ip not in ips:
			ips.append(ip)
		print('Rebro: ',ipport)

def loopFreeProxyCZ():
	#print('Scraping: FreeProxyCZ')
	# for p in proxyList:
	# 	proxy = { "https": p, "http": p}
	# 	print(proxy)
	# 	try:
	# 		r = requests.get('http://free-proxy.cz/en/proxylist/main/date/1', proxies=proxy, timeout=(4,8))
	# 		if 'Free Proxy List' in r.text:
	# 			break
	# 	except: pass

	proxy = { "https": '128.199.68.82:3128', "http": '128.199.68.82:3128'}

	for i in range(1, 151):
		r = requests.get('http://free-proxy.cz/en/proxylist/main/date/' + str(i), proxies=proxy)
		soup = BeautifulSoup(r.text)
		rows = soup.find(id="proxy_list").find('tbody').find_all('tr')
		for row in rows:
			tds = row.find_all('td')
			if tds[0].has_attr('colspan'):
				continue
			ipport = tds[0].get_text().strip() + ':' + tds[1].get_text().strip()
			security = tds[6].get_text().strip()
			if ipport not in ips:
				ips.append(ipport)
			print('FreeProxyCZ: ',ipport)

def loopFile(fileName):
	#print('Scraping: TextFile')
	ipList = []
	if '.dat' in fileName:
		with open(fileName, "rb") as f:
			ipList = pickle.load(f)
	else:
		with open(fileName, "r") as f:
			text = f.read()
			ipList = text.split('\n')

	for ipport in ipList:
		ipport = ipport.strip()
		ip = ipport
		if ip not in ips:
			ips.append(ip)
		print('TextFile '+fileName+': ',ipport)


# def loopNNTime():
# 	for i in range(1, 31):
# 		r = requests.get('http://nntime.com/proxy-list-'+format(i, '02d')+'.htm')
# 		soup = BeautifulSoup(r.text)
# 		rows = soup.find(id="proxy_list").find('tbody').find_all('tr')
# 		for row in rows:
# 			tds = row.find_all('td')
# 			if tds[0].has_attr('colspan'):
# 				continue
# 			ipport = tds[0].get_text().strip() + ':' + tds[1].get_text().strip()
# 			security = tds[6].get_text().strip()
# 			if 'nonymous' in security:
# 				if ipport not in ips:
# 					ips.append(ipport)
# 				print(ipport)

def checkIpsFound():
	for i in range(0,threads):
		t = ThreadUrl(queue)
		t.daemon = True
		t.start()
	#populate queue with data   
	for ip in ips:
		queue.put(ip)

	#wait on the queue until everything has been processed     
	queue.join()

	# for ip in ipsWorking:
	# 	print(ip)

def usage():
	print('\nProxy List Tool - v1.0')
	print('\nUse to get proxies from the internet, merge them with input files,\nit will remove duplicates. You will then have the option have them\nchecked for anon and test against steam community to only pick the\nfastest and output them to a binary file.')
	print('\n\tArguments:\n')
	print('\t-h\tHelp')
	print('\t-g\tGet Proxies from Sites')
	print('\t-a\tAnon Check')
	print('\t-t\tTimeout ex ( -t 0.2,0.4 )')
	print('\t-i\tInput files, comma seperated ex ( -i file1.txt,file2.dat )')
	print('\t-c\tCheck Proxies')
	print('\t-o\tOutput File Name\n')

def main():
	global proxy, ips, ipscount, ipstested, ipsgood, ipsWorking, proxyList, anonCheck, timeout
	with open('proxies.dat', "rb") as f:
		proxyList = pickle.load(f)

	try:
		opts, args = getopt.getopt(sys.argv[1:], "hgcati:o:")
	except getopt.GetoptError:
		usage()
		sys.exit(2)
	
	getFromSites = False
	checkProxies = False
	inputFiles = []
	outputFile = 'proxies.dat'

	for opt, arg in opts:
		if opt in ("-h"):
			usage()
			sys.exit()
		elif opt in ("-g"):
			getFromSites = True
		elif opt in ("-a"):
			anonCheck = True
		elif opt in ("-c"):
			checkProxies = True
		elif opt in ("-i"):
			inputFiles = arg.split(',')
		elif opt in ("-t"):
			timeouts = arg.split(',')
			if len(timeouts) == 2:
				timeout = (timeouts[0], timeouts[1])
			elif len(timeouts) == 1:
				timeout = (timeouts[0], timeouts[0])
		elif opt in ("-o"):
			outputFile = arg

	if getFromSites:
		#loopFreeProxyCZ()
		loopHideMyAss()
		loopCheckerProxy()
		loopRebro()

	for f in inputFiles:
		loopFile(f)

	ipscount = len(ips)

	if checkProxies:
		checkIpsFound()
	else:
		ipsWorking = ips

	if len(ipsWorking) > 0:
		if '.dat' in outputFile:
			with open(outputFile, "wb") as f:
				pickle.dump(ipsWorking, f)
		else:
			with open(outputFile, "w") as f:
				f.write("\n".join(ipsWorking))

	print('Saved ' + str(len(ipsWorking)) + ' ip\'s into ' + outputFile)


if __name__ =='__main__':main()